{
	nixpkgs ? import <nixpkgs> {},
	kevincox-web-compiler ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl "https://kevincox.gitlab.io/kevincox-web-compiler/bin.nixpath")),
}: let
	inherit (nixpkgs) lib pkgs;
	src = lib.fileset.toSource {
		root = ./.;
		fileset = lib.fileset.unions [
			./lib
			./src
		];
	};
in rec {
	www = pkgs.runCommand "kevincox.ca" {
		buildInputs = with pkgs; [file];
	} ''
		${kevincox-web-compiler}/bin/kevincox-web-compiler ${src}/src "$out"
	'';

	www-zip = pkgs.runCommand "kevincox.ca.zip" {} ''
		cd ${www}
		${pkgs.zip}/bin/zip -rv - . >$out
	'';
}

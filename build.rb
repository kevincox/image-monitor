#! /usr/bin/env ruby

require "cgi"
require "erb"
require "open3"
require "pathname"
require "tmpdir"

class ClientContext
	def initialize assets:, asset:
		@_assets = assets
		@_asset = asset
	end

	def _binding
		binding
	end

	def asset path
		@_assets.fetch @_asset.path!.dirname.join(path)
	end
end

module Build_
	ASSETS = {}
	IN = Pathname.new __dir__
	OUT = Pathname.new("build").realdirpath

	ASSET_PREFIX = ENV.fetch "ASSET_PREFIX", "https://cloudflare-ipfs.com"

	begin
		OUT.rmtree
	rescue
	end

	OUT_IPFS = OUT + "ipfs"
	OUT_IPFS.mkpath

	class Asset
		attr_reader :parent
		attr_reader :pin
		attr_reader :type

		def initialize parent:, path:, type: self.class::TYPE
			@parent = parent
			@path = path
			@pin = false
			@type = type
		end

		def path
			pin!
			@path
		end

		def path!
			@path
		end

		def url
			"/#{path}"
		end

		def ipfs_path
			@_ifps_path ||= begin
				tmp = Dir.mktmpdir nil, OUT_IPFS
				basename = path!.basename
				write Pathname.new(tmp) + basename
				hash, status = Open3.capture2 *%W[
					#{__dir__}/ipfs-add.sh -nrQ -- #{tmp}
				]
				raise status unless status.success?
				hash.chomp!
				File.rename tmp, OUT_IPFS + hash
				"/ipfs/#{hash}/#{basename}"
			end
		end

		def ipfs_url
			"#{ASSET_PREFIX}#{ipfs_path}"
		end

		def pin!
			@pin = true
		end

		def write path
			path = OUT + path
			puts "Writing #{path} from #{self}"
			path.dirname.mkpath
			path.write self.render
		end

		def child path
			FORMATS.fetch(path.extname, BinaryAsset)
				.new(parent: self, path: path)
		end

		def preferred_output
			self
		end

		def expand
			[]
		end

		def to_s
			"#<#{self.class.name}:#@type:#{@path.to_s.dump}>"
		end
	end

	class Source
		def initialize path
			@path = path
		end

		def render
			File.read IN + @path
		end

		def asset
			BinaryAsset.new(parent: self, path: @path)
				.child @path
		end
	end

	class BinaryAsset < Asset
		TYPE = :bin

		def initialize **args
			super

			ext = @path.extname
			@type = ext[1..].to_sym unless ext.empty?
		end

		def render format=nil
			raise "Can't convert #{@path.to_s.dump} to #{format.dump}" if format and format != @type

			@parent.render
		end
	end

	class CssAsset < Asset
		TYPE = :css

		def initialize **args
			super
		end

		def render format=nil
			raise "Can't convert #{@path.to_s.dump} to #{format.dump}" if format and format != :css

			@parent.render
		end

		def include
			"<link rel=stylesheet href=\"#{ CGI::escape_html ipfs_url }\"/>"
		end
	end

	class ErbAsset < Asset
		TYPE = :erb

		def render
			path = path!.to_s
			erb = ERB.new File.read IN + path
			erb.filename = path
			Dir.chdir path!.dirname do
				erb.result(ClientContext.new(assets: ASSETS, asset: self)._binding)
			end
		end

		def preferred_output
			base = @path.sub_ext ""
			if base != @path
				child(base)
			else
				self
			end
		end

		def expand
			out = preferred_output
			if out.equal? self
				[]
			else
				[out]
			end
		end
	end

	class JavascriptAsset < Asset
		TYPE = :js

		def initialize **args
			super
		end

		def render format=nil
			raise "Can't convert #{@path.to_s.dump} to #{format.dump}" if format and format != :js

			@parent.render
		end

		def include async: true, module: true
			mod = binding.local_variable_get(:mod)

			"<script#{async ? " async" : ""}#{mod ? " type=module" : ""} src=\"#{ CGI::escape_html ipfs_url }\"></script>"
		end
	end


	FORMATS = {
		".css" => CssAsset,
		".erb" => ErbAsset,
		".js" => JavascriptAsset,
	}
	FORMATS.freeze

	def self.process asset
		path = asset.path!
		raise "Multiple ways to create #{path.to_s.dump}" if ASSETS.include? path
		ASSETS[path] = asset

		asset.expand.each &method(:process)
	end

	def self.process_source path, is_entry
		asset = Source.new(path).asset
		asset.pin! if is_entry
		process asset
	end

	Dir.chdir __dir__
	Pathname.glob "lib/**/*" do |path|
		process_source path, false
	end
	Pathname.glob "src/**/*" do |path|
		process_source path, true
	end


	ASSETS.each_value do |asset|
		if asset.pin
			out = asset.preferred_output
			out.write out.path.to_s.delete_prefix("src/")
		end
	end
end
